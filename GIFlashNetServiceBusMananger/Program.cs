﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.ServiceBus;

namespace GIFlashNetServiceBusMananger
{
    class Program
    {
        private static string _connectionString;
        static void Main(string[] args)
        {
            _connectionString =
                "Endpoint=sb://giflashnet.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=uW6VLyLHXSouQgCjvbBPgP03BpNiHZKxJ4wJ1zaXLFI=";


            //if (args.Length == 0)
            //{
            //    System.Console.WriteLine("Please use the following flags");
            //    System.Console.WriteLine("e <topicname> - Exports all subscriptions to file ");
            //    System.Console.WriteLine("d <filename> <topicname> - Delete all subscriptions in a file a topic by name ");
            //    Console.WriteLine(@"Press any key to close");
            //    Console.ReadLine();
            //    return;

            //}
            //GetAllSubscribers("commandrequest1");
            //if (args[0] == "e")
            //{
            //    GetAllSubscribers(args[1]);
            //}
            //if (args[0] == "d")
            //{
            //    Task.Run(async () =>
            //    {
            //        DeleteSubscribers(args[1], args[2]);
            //    }).GetAwaiter().GetResult();
            //}
            DeleteSubscribers(@"C:\ClientProjects\GIFlashNetServiceBusMananger\GIFlashNetServiceBusMananger\bin\Debug\delete.txt", "commandrequest1");
            Console.ReadLine();

        }

        private static void GetAllSubscribers(string topicName)
        {
            Console.WriteLine($"Writting File {topicName}.csv for Topic {topicName}");
            NamespaceManager namespaceManager = NamespaceManager.CreateFromConnectionString(_connectionString);
            var subscriptions = namespaceManager.GetSubscriptions(topicName);
            using (TextWriter writer = new StreamWriter(@".\" + topicName + ".csv", false, System.Text.Encoding.UTF8))
            {
                var csv = new CsvWriter(writer);
                csv.WriteRecords(subscriptions); // where values implements IEnumerable
            }
            Console.WriteLine($"Done");
        }
        private static async void DeleteSubscribers(string filename, string topicName)
        {
            NamespaceManager namespaceManager = NamespaceManager.CreateFromConnectionString(_connectionString);
            if (!File.Exists(filename))
            {
                Console.WriteLine($@"File not found;");
                return;
            }

            var subscriptions = namespaceManager.GetSubscriptions(topicName).ToList();
            using (TextReader reader = File.OpenText(filename))
            {
                var csv = new CsvReader(reader);
                csv.Configuration.HasHeaderRecord = false;
                var records = csv.GetRecords<SubscriptionName>();
                foreach (var record in records)
                {
                    if (!string.IsNullOrEmpty(record.Name))
                    {
                        var isFound = subscriptions.Where(x => x.Name == record.Name);
                        if (isFound.Any())
                        {
                            Console.WriteLine($"Deleting {record.Name} from {topicName}");
                            await namespaceManager.DeleteSubscriptionAsync(topicName, record.Name);
                        }
                    }              
                }
            }
            Console.ReadLine();
        }

        public class SubscriptionName
        {
            public string Name { get; set; }
        }
    }
}
