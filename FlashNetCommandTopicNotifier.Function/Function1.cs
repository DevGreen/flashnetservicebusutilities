using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.ServiceBus;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace FlashNetServiceBusMonitor.Function
{
    public static class Function1
    {
        private static string _connectionString;
        private const int maxSubscriptions = 2000;
        [FunctionName("DailyCheck")]
        public static async Task RunDaily([TimerTrigger("0 30 8 * * *")]TimerInfo myTimer, TraceWriter log)
        {
            log.Info($"C# Timer trigger function executed at: {DateTime.Now}");
            _connectionString =
                Environment.GetEnvironmentVariable("EndPoint", EnvironmentVariableTarget.Process);
            var topics =
                Environment.GetEnvironmentVariable("Topics", EnvironmentVariableTarget.Process);
            if (topics != null)
            {
                foreach (var topic in topics.Split(','))
                {
                    var topicCount = GetSubscriptionCount(topic);
                    if (maxSubscriptions - topicCount <= 25)
                    {
                        var subject = $"WARNING: {topic} FlashCash Service Bus Topic is almost full!";
                        var body = new StringBuilder();
                        body.AppendFormat("The <b>{0}</b> topic on the giflashnet Service Bus is almost full.<br> It's currently using <b>{1}</b> out of a total <b>{2}</b> available subription slots.", topic, topicCount, maxSubscriptions);
                        body.AppendFormat("<br> Please move some subscriptions to a different topic");
                        body.AppendFormat("<br><br><i>This is an automated message from the FlashNetCommandTopicNotifier Function run in the Azure Portal</i>");
                        var response = await SendEmail(subject, body.ToString());
                        log.Info(response.StatusCode.ToString());
                        log.Info(response.Body.ReadAsStringAsync().Result);
                        log.Info(response.Headers.ToString());
                    }
                }
            }
        }
        [FunctionName("MonthlyCheck")]
        public static async Task RunMonthly([TimerTrigger("0 30 8 1 * *")]TimerInfo myTimer, TraceWriter log)
        {
            log.Info($"C# Timer trigger function executed at: {DateTime.Now}");
            _connectionString =
                Environment.GetEnvironmentVariable("EndPoint", EnvironmentVariableTarget.Process);
            var topics =
                Environment.GetEnvironmentVariable("Topics", EnvironmentVariableTarget.Process);
            var body = new StringBuilder();
            if (topics != null)
            {
                var subject = $"AUTOMATED MONTHY REPORT: FlashCash Service Bus Topic Subscriptions";
                body.AppendFormat("This is a monthly report of subscription counts for FlashCash Service Topics.<br>");
                body.AppendFormat("If the number of subscription exceeds {0} you will experience issues.<br>", maxSubscriptions);

                foreach (var topic in topics.Split(','))
                {
                    var topicCount = GetSubscriptionCount(topic);
                    body.AppendFormat("<br><b>{0}</b>: {1} subscriptions", topic, topicCount);
                }
                body.AppendFormat("<br><br><i>This is an automated message from the FlashNetCommandTopicNotifier Function run in the Azure Portal</i>");
                var response = await SendEmail(subject, body.ToString());
                log.Info(response.StatusCode.ToString());
                log.Info(response.Body.ReadAsStringAsync().Result); 
                log.Info(response.Headers.ToString());
                log.Info($"{subject} email sent");
            }
        }

        private static async Task<Response> SendEmail(string subject, string body)
        {
            var apiKey = Environment.GetEnvironmentVariable("SendGridApi", EnvironmentVariableTarget.Process);
            var emailAddress = Environment.GetEnvironmentVariable("EmailAddress", EnvironmentVariableTarget.Process);

            var client = new SendGridClient(apiKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress("noreply@greenwaldpay.com"),
                Subject = subject,
                HtmlContent = body
            };
            foreach (var email in emailAddress.Split(','))
            {
                msg.AddTo(email);
            }
            Response resp = await client.SendEmailAsync(msg);
            return resp;

        }

        private static int GetSubscriptionCount(string topicName)
        {
            Console.WriteLine($"Writting File {topicName}.csv for Topic {topicName}");
            NamespaceManager namespaceManager = NamespaceManager.CreateFromConnectionString(_connectionString);
            var subscriptions = namespaceManager.GetSubscriptions(topicName);
            return subscriptions.Count();
        }
    }
}
